from django.apps import AppConfig


class StrangenessConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'strangeness'
